package codecommit

import (
	"bytes"
	hm "crypto/hmac"
	"crypto/sha256"
	"fmt"
	"io"
	"net/url"
	"os"
	"path"
	"time"

	"github.com/troyronda/go-git"
	"github.com/troyronda/go-git/clients/common"
	"github.com/troyronda/go-git/clients/http"
)

// CodeCommit contains AWS parameters
type CodeCommit struct {
	accessKeyID     string
	secretAccessKey string
	region          string
}

// NewCodeCommit creates a CodeCommit object with AWS parameters
func NewCodeCommit(accessKeyID, secretAccessKey, region string) *CodeCommit {
	return &CodeCommit{
		accessKeyID,
		secretAccessKey,
		region,
	}
}

// NewCodeCommitFromEnv creates a CodeCommit object with AWS parameters from environment variables
func NewCodeCommitFromEnv(region string) *CodeCommit {
	if region == "" {
		region = os.Getenv("AWS_REGION")
	}

	return &CodeCommit{
		accessKeyID:     os.Getenv("AWS_ACCESS_KEY_ID"),
		secretAccessKey: os.Getenv("AWS_SECRET_ACCESS_KEY"),
		region:          region,
	}
}

// Checkout a repository from AWS CodeCommit
func (cc *CodeCommit) Checkout(repo string, dir string) {
	auth := cc.calculateCredentials(repo)
	checkout(repo, dir, auth)
}

func checkout(repo string, dir string, auth common.AuthMethod) {
	r, err := git.NewRepository(repo, auth)
	if err != nil {
		panic(err)
	}

	if err := r.Pull(git.DefaultRemoteName, "refs/heads/master"); err != nil {
		panic(err)
	}

	hash, err := r.Remotes[git.DefaultRemoteName].Head()
	if err != nil {
		panic(err)
	}

	commit, err := r.Commit(hash)
	if err != nil {
		panic(err)
	}

	fileIter := git.NewFileIter(r, commit.Tree())
	defer fileIter.Close()

	writeFiles(fileIter, dir)
}

func writeFiles(fileIter *git.FileIter, dir string) {
	for {
		file, err := fileIter.Next()
		if err != nil {
			return
		}

		outFilename := dir + file.Name
		basepath := path.Dir(outFilename)

		if os.MkdirAll(basepath, 0777) != nil {
			panic("Unable to create directory!")
		}

		reader, err := file.Reader()
		if err != nil {
			panic(err)
		}
		defer reader.Close()

		outFile, err := os.Create(outFilename)
		defer outFile.Close()
		_, err = io.Copy(outFile, reader)
	}
}

func (cc *CodeCommit) calculateCredentials(repo string) common.AuthMethod {
	// Get credentials (ref: https://medium.com/@nzoschke/codecommit-authentication-helper-in-golang-64f0ef42fc61#.wkl7ljwov)
	u, _ := url.Parse(repo)

	basicAuthUser := cc.accessKeyID
	secretKey := cc.secretAccessKey
	region := cc.region

	basicAuthPassword := credentialHelper(u, region, secretKey)
	return http.NewBasicAuth(basicAuthUser, basicAuthPassword)
}

// Implements codecommit credential-helper:
//   https://github.com/aws/aws-cli/blob/develop/awscli/customizations/codecommit.py#L145
//   $ echo -e "protocol=https\npath=v1/repos/httpd\nhost=git-codecommit.us-east-1.amazonaws.com" | AWS_ACCESS_KEY_ID=AKIAIQF3QUHATF2AY3FQ AWS_SECRET_ACCESS_KEY=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 aws codecommit credential-helper get
//   username=AKIAIQF3QUHATF2AY3FQ
//   password=20160227T172057Z56238a5ac75c8bd36bba91737377aca46c867f584a6695f0486f3e3bba9b4ed5
// References:
//   https://github.com/crowdmob/goamz/blob/master/aws/sign.go

func credentialHelper(u *url.URL, region string, secretKey string) string {
	t := time.Now().UTC()
	// t, _ := time.Parse("20060102T150405", "20160227T172057") // reference time the comments were generated with

	//var regexpCC = regexp.MustCompile(`git-codecommit\.([^.]+)\.amazonaws\.com.*`)
	//if match := regexpCC.FindStringSubmatch(u.Host); len(match) > 1 {
	//	region = match[1]
	//}

	// Build canonical request
	// 'GIT\n/v1/repos/httpd\n\nhost:git-codecommit.us-east-1.amazonaws.com\n\nhost\n'
	cr := new(bytes.Buffer)
	fmt.Fprintf(cr, "%s\n", "GIT")         // HTTPRequestMethod
	fmt.Fprintf(cr, "%s\n", u.Path)        // CanonicalURI
	fmt.Fprintf(cr, "%s\n", "")            // CanonicalQueryString
	fmt.Fprintf(cr, "host:%s\n\n", u.Host) // CanonicalHeaders
	fmt.Fprintf(cr, "%s\n", "host")        // SignedHeaders
	fmt.Fprintf(cr, "%s", "")              // HexEncode(Hash(Payload))

	// Build string to sign
	// 'AWS4-HMAC-SHA256\n20160227T172057\n20160227/us-east-1/codecommit/aws4_request\n650b9e2de2abce7c30f6ad51c4a84b361e1f8aaaa3152e93d35509450db2d869'
	sts := new(bytes.Buffer)
	fmt.Fprint(sts, "AWS4-HMAC-SHA256\n")                                                   // Algorithm
	fmt.Fprintf(sts, "%s\n", t.Format("20060102T150405"))                                   // RequestDate
	fmt.Fprintf(sts, "%s/%s/%s/aws4_request\n", t.Format("20060102"), region, "codecommit") // CredentialScope
	fmt.Fprintf(sts, "%s", hash(cr.String()))                                               // HexEncode(Hash(CanonicalRequest))

	// Calculate the AWS Signature Version 4
	// '56238a5ac75c8bd36bba91737377aca46c867f584a6695f0486f3e3bba9b4ed5'
	dsk := hmac([]byte("AWS4"+secretKey), []byte(t.Format("20060102")))
	dsk = hmac(dsk, []byte(region))
	dsk = hmac(dsk, []byte("codecommit"))
	dsk = hmac(dsk, []byte("aws4_request"))
	h := hmac(dsk, []byte(sts.String()))
	sig := fmt.Sprintf("%x", h) // HexEncode(HMAC(derived-signing-key, string-to-sign))

	// codecommmit smart http password to use with AWS_SECRET_ACCESS_KEY
	return fmt.Sprintf("%sZ%s", t.Format("20060102T150405"), sig)
}

// hash method calculates the sha256 hash for a given string
func hash(in string) string {
	h := sha256.New()
	fmt.Fprintf(h, "%s", in)
	return fmt.Sprintf("%x", h.Sum(nil))
}

// hmac method calculates the sha256 hmac for a given slice of bytes
func hmac(key, data []byte) []byte {
	h := hm.New(sha256.New, key)
	h.Write(data)
	return h.Sum(nil)
}
